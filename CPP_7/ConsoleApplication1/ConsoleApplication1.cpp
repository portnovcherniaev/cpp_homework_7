#include <iostream>
#include <stack> 

using namespace std;


int main()
{
	int N;
	cout << "enter how many elements will be in stack"<<'\n';
	cin >> N;

	stack<int> hwstack;
	for (int i = 0; i < N; i++) hwstack.push(i);

	cout << "popping out elements of your stack: "<<'\n';
	while (!hwstack.empty())
	{
		cout <<' '<< hwstack.top ();
		hwstack.pop();
	}

	return 0;
}